package com.acto.desafioActo.exception;

public class WorkerAlreadyExistsException extends RuntimeException{

    public WorkerAlreadyExistsException(String cpf) {
        super("CPF: " + cpf + "já existente.");
    }
}
