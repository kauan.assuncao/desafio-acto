package com.acto.desafioActo.exception;

import com.acto.desafioActo.model.Role;

import java.util.List;

public class RolesAlreadyExistsException extends RuntimeException {

    public RolesAlreadyExistsException(List<Role> roles) {
        super("Cargo(s) já existente(s): " + roles);
    }

}
