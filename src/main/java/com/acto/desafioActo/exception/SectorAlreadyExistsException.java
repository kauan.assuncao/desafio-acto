package com.acto.desafioActo.exception;

public class SectorAlreadyExistsException extends RuntimeException {

    public SectorAlreadyExistsException(String name) {
        super("Setor " + name + " Existente.");
    }


}
