package com.acto.desafioActo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioActoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioActoApplication.class, args);
	}

}
