package com.acto.desafioActo.dto;

public class SucessoResponse {

    private String mensagem;

    public SucessoResponse(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
