package com.acto.desafioActo.dto;

import com.acto.desafioActo.model.Role;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SectorRequest {

    private int id;
    private String nome;
    private List<Role> roles;
}
