package com.acto.desafioActo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkerRequest {

    private int id;
    private String name;
    private String cpf;
}
