package com.acto.desafioActo.service;

public interface RoleService {

    public boolean alreadyExists(String roleName);
}
