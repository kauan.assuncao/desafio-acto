package com.acto.desafioActo.service.validate;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.exception.RolesAlreadyExistsException;
import com.acto.desafioActo.exception.SectorAlreadyExistsException;
import com.acto.desafioActo.exception.WorkerAlreadyExistsException;
import com.acto.desafioActo.model.Role;
import com.acto.desafioActo.service.impl.RoleServiceImpl;
import com.acto.desafioActo.service.repository.SectorRepository;
import com.acto.desafioActo.service.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class ValidationMethod {

    @Autowired
    SectorRepository sectorRepository;

    @Autowired
    private RoleServiceImpl roleServiceImpl;

    @Autowired
    WorkerRepository workerRepository;

    public String validateSector(SectorRequest setorRequest) {
        boolean sectorExists = sectorRepository.existsByNome(setorRequest.getNome());

        if (sectorExists) {
            throw new SectorAlreadyExistsException(setorRequest.getNome());
        }

        List<Role> existentsRoles = setorRequest.getRoles().stream()
                .filter(role -> roleServiceImpl.alreadyExists(role.getName())).collect(Collectors.toList());

        if (!existentsRoles.isEmpty()) {
            throw new RolesAlreadyExistsException(existentsRoles);
        }
        return "Validação concluída";
    }

    public String validateWorker(WorkerRequest workerRequest) {
        boolean workerExists = alreadyExists(workerRequest.getCpf());

        if (workerExists) {
            throw new WorkerAlreadyExistsException(workerRequest.getCpf());
        }
        return "Validação concluída";
    }

    private boolean alreadyExists(String workerCpf) {
        return workerRepository.existsByCpf(workerCpf);
    }

}
