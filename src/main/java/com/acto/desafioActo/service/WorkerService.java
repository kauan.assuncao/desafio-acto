package com.acto.desafioActo.service;

import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.model.Worker;

public interface WorkerService {

    public Worker workerRegister(WorkerRequest workerRequest);
}
