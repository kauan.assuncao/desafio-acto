package com.acto.desafioActo.service.mapping;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.model.Sector;
import com.acto.desafioActo.model.Worker;
import org.mapstruct.factory.Mappers;

@org.mapstruct.Mapper
public interface Mapper {

    Mapper INSTANCE = Mappers.getMapper(Mapper.class);

    public Sector toSetor(SectorRequest setorRequest);

    public Worker toWorker(WorkerRequest workerRequest);

}
