package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.service.RoleService;
import com.acto.desafioActo.service.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    public boolean alreadyExists(String roleName) {
        return roleRepository.existsByName(roleName);
    }
}
