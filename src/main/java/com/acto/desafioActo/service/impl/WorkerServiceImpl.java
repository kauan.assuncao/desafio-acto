package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.model.Worker;
import com.acto.desafioActo.service.WorkerService;
import com.acto.desafioActo.service.mapping.Mapper;
import com.acto.desafioActo.service.repository.WorkerRepository;
import com.acto.desafioActo.service.validate.ValidationMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class WorkerServiceImpl implements WorkerService {

    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    ValidationMethod validationMethod;

    private Worker worker;

    @Transactional
    public Worker workerRegister(WorkerRequest workerRequest) {

        validationMethod.validateWorker(workerRequest);

        worker = Mapper.INSTANCE.toWorker(workerRequest);

        return workerRepository.save(worker);
    }
}


