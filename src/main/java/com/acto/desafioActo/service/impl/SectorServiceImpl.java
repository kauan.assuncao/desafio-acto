package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.model.Sector;
import com.acto.desafioActo.service.SectorService;
import com.acto.desafioActo.service.mapping.Mapper;
import com.acto.desafioActo.service.repository.SectorRepository;
import com.acto.desafioActo.service.validate.ValidationMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SectorServiceImpl implements SectorService {

    @Autowired
    SectorRepository sectorRepository;

    @Autowired
    ValidationMethod validationMethod;

    private Sector setor;

    @Transactional
    public Sector register(SectorRequest sectorRequest) {

        validationMethod.validateSector(sectorRequest);

        setor = Mapper.INSTANCE.toSetor(sectorRequest);

        return sectorRepository.save(setor);
    }

    @Transactional
    public Iterable<Sector> findALl() {
        return sectorRepository.findAll();
    }



}
