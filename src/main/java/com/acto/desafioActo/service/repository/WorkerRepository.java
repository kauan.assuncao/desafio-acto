package com.acto.desafioActo.service.repository;

import com.acto.desafioActo.model.Worker;
import org.springframework.data.repository.CrudRepository;

public interface WorkerRepository extends CrudRepository<Worker, Integer> {

    boolean existsByCpf(String cpf);
}
