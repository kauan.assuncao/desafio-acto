package com.acto.desafioActo.service.repository;

import com.acto.desafioActo.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    boolean existsByName(String name);
}
