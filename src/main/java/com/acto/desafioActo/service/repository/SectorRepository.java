package com.acto.desafioActo.service.repository;

import com.acto.desafioActo.model.Sector;
import org.springframework.data.repository.CrudRepository;

public interface SectorRepository extends CrudRepository<Sector, Integer> {

    Sector findOneByNome(String nome);

    boolean existsByNome(String nome);
}
