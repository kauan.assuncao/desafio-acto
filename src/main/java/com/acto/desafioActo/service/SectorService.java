package com.acto.desafioActo.service;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.model.Sector;

public interface SectorService {

    Sector register(SectorRequest setorRequest);
}
