package com.acto.desafioActo.controller;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.dto.SucessoResponse;
import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.service.impl.WorkerServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/worker")
@Api("Api REST Worker")
@CrossOrigin(origins = "*")
public class WorkerController {

    @Autowired
    private WorkerServiceImpl workerServiceImpl;

    @PostMapping
    @ApiOperation("Retorna se o registro do trabalhador foi executado ou não")
    public ResponseEntity<Object> workerRegister(@RequestBody WorkerRequest workerRequest) {

        return ResponseEntity.ok(workerServiceImpl.workerRegister(workerRequest));
    }

}
