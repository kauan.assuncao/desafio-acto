package com.acto.desafioActo.controller;

import com.acto.desafioActo.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/role")
@Api("Api REST Role")
@CrossOrigin(origins = "*")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("verifyExists/{roleName}")
    @ApiOperation("Retorna se o nome do cargo existe no cadastro")
    public ResponseEntity<Boolean> verifyExists(@PathParam("roleName") String roleName) {
        return ResponseEntity.ok(roleService.alreadyExists(roleName));
    }
}
