package com.acto.desafioActo.controller;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.dto.SucessoResponse;
import com.acto.desafioActo.model.Sector;
import com.acto.desafioActo.service.impl.SectorServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sector")
@Api("Api REST Sector")
@CrossOrigin(origins = "*")
public class SectorController {

    @Autowired
    private SectorServiceImpl sectorServiceImpl;

    @PostMapping()
    @ApiOperation("Retorna se o cadastro do setor é executado ou não")
    public ResponseEntity<Object> register(@RequestBody SectorRequest setorRequest) {
            sectorServiceImpl.register(setorRequest);

            return ResponseEntity.ok().body(new SucessoResponse("Setor adicionado com sucesso!"));
    }

    @GetMapping()
    @ApiOperation("Retorna lista de setores")
    public ResponseEntity<Iterable<Sector>> findAll(@RequestBody SectorRequest setorRequest) {

        return ResponseEntity.ok(sectorServiceImpl.findALl());
    }

}
