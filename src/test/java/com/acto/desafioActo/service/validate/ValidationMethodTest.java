package com.acto.desafioActo.service.validate;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.model.Role;
import com.acto.desafioActo.service.impl.RoleServiceImpl;
import com.acto.desafioActo.service.repository.SectorRepository;
import com.acto.desafioActo.service.repository.WorkerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ValidationMethodTest {

    @InjectMocks
    ValidationMethod validationMethod;

    @Mock
    SectorRepository sectorRepository;

    @Mock
    RoleServiceImpl roleServiceImpl;

    @Mock
    WorkerRepository workerRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void validateSectorIfTest() {
        SectorRequest sectorRequest = Mockito.mock(SectorRequest.class);

        try {
        Mockito.when(sectorRepository.existsByNome(sectorRequest.getNome())).thenReturn(true);

        validationMethod.validateSector(sectorRequest);
        } catch (Exception e) {
             assertEquals("Setor " + sectorRequest.getNome() + " Existente.", e.getMessage());
        }

    }

    @Test
    public void validateSectorIf2Test() {
        SectorRequest sectorRequest = Mockito.mock(SectorRequest.class);
        Role role = Mockito.mock(Role.class);
        List<Role> listRole = new ArrayList<>();
        listRole.add(role);

        try {
            Mockito.when(sectorRepository.existsByNome(sectorRequest.getNome())).thenReturn(false);
            Mockito.when(roleServiceImpl.alreadyExists(role.getName())).thenReturn(true);

            validationMethod.validateSector(sectorRequest);
        } catch (Exception e) {
            assertEquals("Cargo(s) já existente(s): ", e.getMessage());
        }

    }


    @Test
    public void validateSectorTest() {
        SectorRequest sectorRequest = Mockito.mock(SectorRequest.class);
        Role role = Mockito.mock(Role.class);
        List<Role> listRole = new ArrayList<>();
        listRole.add(role);

        Mockito.when(sectorRepository.existsByNome(sectorRequest.getNome())).thenReturn(false);
        Mockito.when(roleServiceImpl.alreadyExists(role.getName())).thenReturn(false);

        assertEquals("Validação concluída", validationMethod.validateSector(sectorRequest));

    }

    @Test
    public void validateWorkerIfTest() {
        WorkerRequest workerRequest = Mockito.mock(WorkerRequest.class);

        try {
            Mockito.when(workerRepository.existsByCpf(Mockito.anyString())).thenReturn(true);

            validationMethod.validateWorker(workerRequest);
        } catch (Exception e) {
            assertEquals("CPF: " + workerRequest.getCpf() + "já existente.", e.getMessage());
        }

    }


    @Test
    public void validateWorkerTest() {
        WorkerRequest workerRequest = Mockito.mock(WorkerRequest.class);

        Mockito.when(workerRepository.existsByCpf(Mockito.anyString())).thenReturn(false);

        assertEquals("Validação concluída", validationMethod.validateWorker(workerRequest));
    }
}
