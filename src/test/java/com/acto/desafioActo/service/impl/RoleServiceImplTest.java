package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.service.repository.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RoleServiceImplTest {

    @InjectMocks
    RoleServiceImpl roleServiceImpl;

    @Mock
    RoleRepository roleRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void alreadyExistsTest() {
        String roleName = "Test";

        Mockito.when(roleRepository.existsByName(roleName)).thenReturn(true);

        assertEquals(true, roleServiceImpl.alreadyExists(roleName));
    }
}
