package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.dto.WorkerRequest;
import com.acto.desafioActo.model.Worker;
import com.acto.desafioActo.service.mapping.Mapper;
import com.acto.desafioActo.service.repository.WorkerRepository;
import com.acto.desafioActo.service.validate.ValidationMethod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WorkerServiceImplTest {

    @InjectMocks
    WorkerServiceImpl workerServiceImpl;

    @Mock
    WorkerRepository workerRepository;

    @Mock
    ValidationMethod validationMethod;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void workerRegisterTest() {
        Worker worker = Mockito.mock(Worker.class);
        WorkerRequest workerRequest = Mockito.mock(WorkerRequest.class);

        Mockito.when(validationMethod.validateWorker(Mockito.any())).thenReturn("Test");
        Mockito.when(workerRepository.save(Mockito.any())).thenReturn(worker);

        assertEquals(worker, workerServiceImpl.workerRegister(workerRequest));
    }
}
