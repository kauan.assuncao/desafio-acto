package com.acto.desafioActo.service.impl;

import com.acto.desafioActo.dto.SectorRequest;
import com.acto.desafioActo.model.Sector;
import com.acto.desafioActo.service.repository.SectorRepository;
import com.acto.desafioActo.service.validate.ValidationMethod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SectorServiceImplTest {

    @InjectMocks
    SectorServiceImpl sectorServiceImpl;

    @Mock
    SectorRepository sectorRepository;

    @Mock
    ValidationMethod validationMethod;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void registerTest() {
        Sector setor = Mockito.mock(Sector.class);
        SectorRequest sectorRequest = Mockito.mock(SectorRequest.class);

        Mockito.when(validationMethod.validateSector(Mockito.any())).thenReturn("Test");
        Mockito.when(sectorRepository.save(Mockito.any())).thenReturn(setor);

        assertEquals(setor, sectorServiceImpl.register(sectorRequest));
    }
}
